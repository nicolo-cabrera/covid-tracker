import {MDBContainer,MDBRow, MDBCol, MDBCard, MDBCardBody,MDBCardText,MDBCardTitle} from 'mdbreact'
import formatNumber from '../helpers/formatNumber'


export default function Home({data}) {

  return (

    <MDBContainer className="home-container mt-5 d-flex justify-content-center align-items-center">
      <MDBContainer>
          <h1 className="text-center pt-3 headers">Global Case</h1>
          <MDBRow>
            <MDBCol md="6">
              <MDBCard className="mt-3">
                <MDBCardBody>
                <MDBCardTitle className="text-center py-1 active-total">{formatNumber(data.total)}</MDBCardTitle>
                  <MDBCardText className="text-center">
                    Coronavirus Cases
                  </MDBCardText>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
            <MDBCol md="6">
              <MDBCard className="mt-3">
                <MDBCardBody>
                  <MDBCardTitle className="text-center py-1 active-total">{formatNumber(data.activeCases.total)}</MDBCardTitle>
                  <MDBCardText className="text-center">
                    Active Cases
                  </MDBCardText>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol md="6">
              <MDBCard className="mt-3">
                <MDBCardBody>
                <MDBCardTitle className="text-center py-1 critical">{formatNumber(data.activeCases.critical)}</MDBCardTitle>
                  <MDBCardText className="text-center">
                    Critical
                  </MDBCardText>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
            <MDBCol md="6">
              <MDBCard className="mt-3">
                <MDBCardBody>
                  <MDBCardTitle className="text-center py-1 recovered">{formatNumber(data.closedCases.recovered)}</MDBCardTitle>
                    <MDBCardText className="text-center">
                      Recovered
                    </MDBCardText>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol md="6">
              <MDBCard className="mt-3">
                <MDBCardBody>
                <MDBCardTitle className="text-center py-1 death">{formatNumber(data.closedCases.deaths)}</MDBCardTitle>
                  <MDBCardText className="text-center">
                    Deaths
                  </MDBCardText>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
            <MDBCol md="6">
              <MDBCard className="mt-3">
                <MDBCardBody>
                  <MDBCardTitle className="text-center py-1 text-center mild">{formatNumber(data.activeCases.mild)}</MDBCardTitle>
                    <MDBCardText className="text-center">
                      Mild
                    </MDBCardText>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
    </MDBContainer>
  )
}

export async function getServerSideProps() {

  const res = await fetch(`https://covid19-update-api.herokuapp.com/api/v1/cases`)
  const data = await res.json()
  return {
    props : {
      data
    }
  }
}