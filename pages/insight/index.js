import {Line} from 'react-chartjs-2';
import {MDBContainer,MDBRow, MDBCol} from 'mdbreact'

export default function Insight({totalCases, dailyCases, totalDeaths, dailyDeaths}) {


  const total = {
    labels : totalCases.graph.categories,
    datasets : [
      {
        label : totalCases.graph.title,
       data : totalCases.graph.data,
       backgroundColor: 'rgba(75,192,192,1)',
       borderColor: 'rgba(0,0,0,1)',
       borderWidth: 2

      }
    ]
  }

  const daily = {
    labels : dailyCases.graph.categories,
    datasets : [
      {
        label : dailyCases.graph.title,
       data : dailyCases.graph.data,
       backgroundColor: "blue",
       borderColor: 'rgba(0,0,0,1)',
       borderWidth: 2

      }
    ]
  }

  const totalDeath = {
    labels : totalDeaths.graph.categories,
    datasets : [
      {
        label : totalDeaths.graph.title,
       data : totalDeaths.graph.data,
       backgroundColor: "red",
       borderColor: 'rgba(0,0,0,1)',
       borderWidth: 2

      }
    ]
  }

  const dailyDeath = {
    labels : dailyDeaths.graph.categories,
    datasets : [
      {
        label : dailyDeaths.graph.title,
       data : dailyDeaths.graph.data,
       backgroundColor: "indianred",
       borderColor: 'rgba(0,0,0,1)',
       borderWidth: 2

      }
    ]
  }



  return(

      <MDBContainer className="container-insight mt-5 d-flex justify-content-center align-items-center">
        <MDBContainer>
          <MDBRow className="mt-5">
            <MDBCol md="6">
              <h1 className="text-center">Total Cases</h1>
              <Line data={total}/>
            </MDBCol>
            <MDBCol md="6">
              <h1 className="text-center">Daily Cases</h1>
              <Line data={daily}/>
            </MDBCol>
          </MDBRow>
          <MDBRow className="mt-5">
            <MDBCol md="6">
              <h1 className="text-center">Total Deaths</h1>
              <Line data={totalDeath}/>
            </MDBCol>
            <MDBCol md="6">
              <h1 className="text-center">Daily Deaths</h1>
              <Line data={dailyDeath}/>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </MDBContainer>

    )
}

export async function getServerSideProps() {

  const res = await fetch("https://covid19-update-api.herokuapp.com/api/v1/cases/graphs/totalCases")
  const totalCases = await res.json()

  const res2 = await fetch("https://covid19-update-api.herokuapp.com/api/v1/cases/graphs/dailyCases")
  const dailyCases = await res2.json()

  const res3 = await fetch("https://covid19-update-api.herokuapp.com/api/v1/death/graph/totalDeaths")
  const totalDeaths = await res3.json()

  const res4 = await fetch("https://covid19-update-api.herokuapp.com/api/v1/death/graph/dailyDeaths")
  const dailyDeaths = await res4.json()
  return {
    props : {
      totalCases,
      dailyCases,
      totalDeaths,
      dailyDeaths

    }
  }
}