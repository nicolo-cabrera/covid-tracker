import {useState} from 'react'
import { MDBListGroup, MDBListGroupItem, MDBRow, MDBCol, MDBContainer, MDBInput, MDBBtn  } from "mdbreact"
import Link from 'next/link'
import Fuse from 'fuse.js'

export default function Countries({data}) {

	const [search, setSearch] = useState("")

	const countries = data.countries.map(country => {
		return {
			name : country.name,
			continent : country.continent
		}
	})

	const fuse = new Fuse(countries, {
	keys: [
	  'name',
	  'continent',

	],
	includeScore: true
	});

	const results = fuse.search(search);
	const countryResults = search !== "" ? results.map(country => country.item) : countries;

	return(

			<MDBContainer className="countries-container mt-5">
				<MDBRow>
					<MDBCol>
						<MDBInput label="Type here to search for a country..." value={search} onChange={e => setSearch(e.target.value)}/>
					</MDBCol>
				</MDBRow>
				<MDBRow>
					<MDBCol>
						<MDBListGroup>
						 {
						 	countryResults.map(country => {
						 		return (
						 				<Link href="/countries/[country]" as={`/countries/${country.name.replace( /\s/g, '') }`} key={country.name} >
								 				<a>
									 				<MDBListGroupItem>
									 					{country.name}
									 				</MDBListGroupItem>
								 				</a>
							 			</Link>
						 			)
						 	})
						 }
						</MDBListGroup>
					</MDBCol>
				</MDBRow>
			</MDBContainer>

		)
}

export async function getStaticProps() {

	const res = await fetch("https://covid19-update-api.herokuapp.com/api/v1/world")
	const data = await res.json()
	

  return {
    props: {

    	data

    }
  }
}