import {useState, useEffect, useRef} from 'react'
import {MDBContainer,MDBRow, MDBCol, MDBCard, MDBCardBody,MDBCardText,MDBCardTitle} from "mdbreact"
import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY

import formatNumber from '../../helpers/formatNumber'

export default function Country({country}) {

	const mapContainerRef = useRef(null)
	const [latitude, setLatitude] = useState(0)
	const [longitude, setLongitude] = useState(0)
	

	useEffect(() => {
		const map = new mapboxgl.Map({


			container : mapContainerRef.current,

			style : 'mapbox://styles/mapbox/outdoors-v11',
			center: [longitude, latitude],

			zoom : 3
		})

	
		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

		const marker = new mapboxgl.Marker()
		.setLngLat([longitude, latitude])
		.addTo(map)


		fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${country.name}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
		.then(res => res.json())
		.then(data => {
			setLongitude(data.features[0].center[0]) 
			setLatitude(data.features[0].center[1])
			
		})

		return () => map.remove()


	},[latitude,longitude])


	return(

			<MDBContainer className="country-container mt-5 d-flex justify-content-center align-items-center">
				<MDBContainer>
					<h1 className="text-center mb-5 headers">{country.name}</h1>
					<MDBRow>
						<MDBCol md="6">
							<div className="mapContainer" ref={mapContainerRef}/>
						</MDBCol>
						<MDBCol md="6">
						  <MDBCard className="mt-3">
						    <MDBCardBody>
						    <MDBCardTitle className="text-center py-1 active-total">{formatNumber(country.cases)}</MDBCardTitle>
							    <MDBCardText className="text-center">
							      Cases
							    </MDBCardText>
						    </MDBCardBody>
						  </MDBCard>
						  <MDBCard className="mt-3">
						    <MDBCardBody>
						      <MDBCardTitle className="text-center py-1 death">{formatNumber(country.deaths)}</MDBCardTitle>
						      <MDBCardText className="text-center">
						        Deaths
						      </MDBCardText>
						    </MDBCardBody>
						  </MDBCard>
						</MDBCol>
					</MDBRow>
				</MDBContainer>
			</MDBContainer>
		

		)
}

export async function getServerSideProps({params}) {
	
  const res = await fetch("https://covid19-update-api.herokuapp.com/api/v1/world")
  const data = await res.json()
  const country = data.countries.find(result => result.name.replace( /\s/g, '')  === params.country)

  return {
   props: {
   		 country
     } 
	}
}