import {useState, useEffect} from 'react'
import Router from 'next/router'
import Head from 'next/head'

import '@fortawesome/fontawesome-free/css/all.min.css'
import'bootstrap-css-only/css/bootstrap.min.css'
import'mdbreact/dist/css/mdb.css'
import '../styles/globals.css'

import 'mapbox-gl/dist/mapbox-gl.css/'


/*components*/
import NavBar from '../components/NavBar'
import Footer from '../components/Footer'
import Loader from '../components/Loader'





function MyApp({ Component, pageProps }) {

	const [isLoading, setIsLoading] = useState(false)




Router.events.on('routeChangeStart', () => {
  	setIsLoading(true)
});

Router.events.on('routeChangeComplete', () => {
  	setIsLoading(false)
});

Router.events.on('routeChangeError', () => {
  	setIsLoading(false)
});



  return (

  	<>
  		<Head>
  			<title>Covid Tracker</title>
  			<meta charSet="UTF-8"/>
  			<meta name="description" content="Covid Tracker" />
  			<meta name="keywords" content="Covid, Covid Tracker" />
  			<meta name="viewport" content="width=device-width, initial-scale=1.0" />
  		</Head>
	  	<NavBar />
	  	{
	  		isLoading ? 
	  		<Loader />
	  		:
	  		<Component {...pageProps} />
	  	}
	  	<Footer />
  	</>

  	)
}

export default MyApp
