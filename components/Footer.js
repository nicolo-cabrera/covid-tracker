import { MDBCol, MDBContainer, MDBRow, MDBFooter, MDBIcon } from "mdbreact";


export default function Footer() {
	return (

		<MDBFooter color="blue" className="font-small pt-4 mt-4">
		  <MDBContainer fluid className="d-flex  flex-column justify-content-center">
		    <MDBRow>
		      <MDBCol md="6" className="d-flex flex-column justify-content-center align-items-center">
		        <h5 className="title">Contact</h5>
		        <div>
		          <div>
		            <MDBIcon icon="at" /> Gmail : cabrera.jonnicolo@gmail.com
		          </div>
		        </div> 
		      </MDBCol>
		      <MDBCol md="6" className="d-flex flex-column justify-content-center align-items-center">
		        <h5 className="title">Links</h5>
		          <div>
		            <div>
		              <a href="https://gitlab.com/nicolo-cabrera" target="_blank">Gitlab</a>
		            </div>
		            <div>
		              <a href="https://covid19-update-api.herokuapp.com/api/v1" target="_blank">API</a>
		            </div>
		            <div>
		              <a href="https://www.npmjs.com/package/react-chartjs-2" target="_blank">Chart</a>
		            </div>
		            <div>
		              <a href="https://www.mapbox.com/" target="_blank">Map</a>
		            </div>
		          </div> 
		      </MDBCol>
		    </MDBRow>
		  </MDBContainer>
		  <div className="text-center py-3">
		    <MDBContainer fluid>
		      &copy; {new Date().getFullYear()} <a href="https://nicolo-cabrera.vercel.app/" target="_blank">Copyright: Jon Nicolo B. Cabrera</a>
		    </MDBContainer>
		  </div>
		</MDBFooter>

		)
}