import {MDBContainer} from 'mdbreact'

export default function Loader() {
	return(

			<MDBContainer className="loader-container d-flex justify-content-center align-items-center">
				<div className="loader" />
			</MDBContainer>


		)
}