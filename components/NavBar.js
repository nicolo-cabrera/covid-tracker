import {useState} from 'react'

import Link from 'next/link'
import { useRouter } from "next/router"
import {
MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBFormInline,
MDBDropdown, MDBDropdownToggle} from "mdbreact";


export default function NavBar() {

	const [isOpen, setIsOpen] = useState(false)
	const router = useRouter()

	return (


			  <MDBNavbar color="blue" dark expand="md" fixed="top">
			  	<Link href="/">
			  		<a className="navbar-brand">Covid Tracker</a>
			  	</Link>
			    <MDBNavbarToggler onClick={() => setIsOpen(!isOpen)} />
			    <MDBCollapse id="navbarCollapse3" isOpen={isOpen} navbar>
			      <MDBNavbarNav right>
			      	<Link href="/">
			      		<a className={router.pathname == "/" ? "nav-link text-white active" : "nav-link text-white"}>Home</a>
			      	</Link>
			      	<Link href="/insight">
			      		<a className={router.pathname == "/insight" ? "nav-link text-white active" : "nav-link text-white"}>Insight</a>
			      	</Link>
			      	<Link href="/countries">
			      		<a className={router.pathname == "/countries" ? "nav-link text-white active" : "nav-link text-white"}>Search Country</a>
			      	</Link>
			      </MDBNavbarNav>
			    </MDBCollapse>
			  </MDBNavbar>


		)
}